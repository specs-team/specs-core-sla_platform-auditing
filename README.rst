.. contents:: Table of Contents

========
Overview
========

The SPECS Auditing component (designed under the umbrella of Enforcement module) serves as a logging/auditing component for the entire SPECS framework. The component is implemented as a maven project with two modules:

- Audit Server: provides REST API for storing, retrieving and searching for audit events, interacts with the underlying audit database.
- Audit Client: Java library that offers Java API with convenience methods for creating and storing audit events which interacts in the background with the audit-server.

The component depends on the common data model classes defined in the specs-utility-data-model project.

The component supports following audit event types:

- component activity
- service activity
- diagnosed monitoring event
- remediation result
- end user notification

============
Installation
============

The Auditing component can be downloaded from the SPECS maven repository::

 https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots/eu/specs-project/core/sla-platform/auditing/

The Auditing component can also be built from source code using the Apache Maven 3 tool. First clone the project from the Bitbucket repository using a Git client::

 git clone git@bitbucket.org:specs-team/specs-core-sla_platform-auditing.git

then go into the cloned directory and run::

 mvn package

The command produces following two artifacts:

- audit-server.war
- audit-client.jar

------------
Audit Server
------------

Prerequisites:

- Java web container
- MongoDB
- Java 7

The Audit server is packaged as a web application archive (war) file with the name audit-server.war which has to be deployed to a Java web container. For example, to deploy the application to Apache Tomcat, just copy the war file to the Tomcat webapps directory::

 cp audit-server/target/audit-server.war /var/lib/tomcat7/webapps/

The application configuration is located in the file *audit-server.properties* in the Java properties format. The file contains the following configuration properties:

.. code-block:: jproperties

 mongodb.host=localhost
 mongodb.port=27017
 mongodb.database=auditing

Make the necessary changes if needed and restart the web container for changes to take effect. The Audit server should now be available at::

 https://<host>:<port>/audit-server

------------
Audit Client
------------

The Audit client is a Java library that can be used by other SPECS components to create and publish audit events. When using Apache Maven, the Audit client can be simply added to project's dependencies::

 <dependency>
    <groupId>eu.specs-project.core.sla-platform.auditing</groupId>
    <artifactId>audit-client</artifactId>
    <version>0.0.1-SNAPSHOT</version>
 </dependency>

Otherwise the jar file audit-client.jar has to be included to the project's classpath.

In non-Java projects where the Audit client library cannot be used, the audit events can be created in accordance with the audit event schema and published by calling Audit server REST API.

Audit client requires following configuration properties:

- *audit_client.audit_server_address*: audit server address
- *audit_client.keystore.path*: path of the Java key store with the component certificate
- *audit_client.keystore.password*: key store password
- *audit_client.truststore.path*: path of the Java trust store with the SPECS certificate authority certificate chain
- *audit_client.truststore.password*: trust store password

=====
Usage
=====

At the application startup initialize the Audit client using the *AuditorFactory* init method by providing the properties file path::

 void AuditorFactory.init(String configFilePath);

The *AuditorFactory* creates an *Auditor* instance and caches it in memory. The *Auditor* is now ready to accept audit events and publish them to the Audit server.
*Auditor* can be retrieved from the *AuditorFactory* using the *getAuditor* method::

 Auditor auditor = AuditorFactory.getAuditor();

If using Spring framework, the *Auditor* can be defined in the Spring context file instead:

.. code-block:: xml

 <bean id="auditor" class="eu.specsproject.core.slaplatform.auditing.AuditClient">
    <constructor-arg name="auditServerAddress" value=" https://localhost/audit-server"/>
    ...
 </bean>

The *Auditor* bean can then be injected using the *Autowired* stereotype.

To audit an event create an *AuditEvent* object, populate it with relevant data and send it using the *Auditor*'s *audit* method::

 AuditEvent auditEvent = new AuditEvent();
 auditEvent.set... // set relevant data
 auditor.audit(auditEvent);

======
Notice
======

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

- http://www.specs-project.eu/
- http://www.xlab.si/

Developers:

- Jolanda Modic, jolanda.modic@xlab.si
- Damjan Murn, damjan.murn@xlab.si

Copyright:

.. code-block:: 

 Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

 SPECS Auditing is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License, version 3,
 as published by the Free Software Foundation.

 SPECS Auditing is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.