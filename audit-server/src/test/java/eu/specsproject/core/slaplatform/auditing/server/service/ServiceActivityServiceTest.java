package eu.specsproject.core.slaplatform.auditing.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ServiceActivity;
import eu.specsproject.core.slaplatform.auditing.server.TestParent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:audit-server-context-test.xml")
public class ServiceActivityServiceTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(ServiceActivityServiceTest.class);

    @Autowired
    private ServiceActivityService saService;

    @Test
    public void testService() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        ServiceActivity ServiceActivity1 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/service-activity.json"), ServiceActivity.class);

        String saId1 = saService.save(ServiceActivity1);

        ServiceActivity ServiceActivity1a = saService.findById(saId1);
        assertEquals(ServiceActivity1a.getId(), saId1);
        assertEquals(ServiceActivity1a.getSlaId(), ServiceActivity1.getSlaId());

        ServiceActivity ServiceActivity2 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/service-activity.json"), ServiceActivity.class);
        ServiceActivity2.setId(UUID.randomUUID().toString());
        String saId2 = saService.save(ServiceActivity2);

        ServiceActivityService.Filter filter = new ServiceActivityService.Filter();
        List<ServiceActivity> caIdList = saService.findAll(filter);
        assertEquals(caIdList.size(), 2);
        assertEquals(caIdList.get(0).getId(), saId1);
        assertEquals(caIdList.get(1).getId(), saId2);
    }
}