package eu.specsproject.core.slaplatform.auditing.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fakemongo.Fongo;
import com.mongodb.DB;
import eu.specs.datamodel.enforcement.CompActivity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:audit-server-context-test.xml")
public class CompActivityControllerTest {
    private static final Logger logger = LogManager.getLogger(CompActivityControllerTest.class);

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Autowired
    public Fongo fongo;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
        this.objectMapper = new ObjectMapper();
    }

    @After
    public void tearDown() throws Exception {
        for (DB db : fongo.getUsedDatabases()) {
            logger.trace("Dropping database " + db.getName());
            db.dropDatabase();
        }
    }

    @Test
    public void testApi() throws Exception {

        CompActivity compActivity1 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/comp-activity.json"), CompActivity.class);
        CompActivity compActivity2 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/comp-activity.json"), CompActivity.class);
        compActivity2.setId(UUID.randomUUID().toString());

        MvcResult result1 = mockMvc.perform(post("/comp-activities")
                .content(objectMapper.writeValueAsString(compActivity1)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        String caUri1 = result1.getResponse().getHeaderValue("Location").toString();
        Pattern pattern = Pattern.compile("/([\\w-]+)$");
        Matcher m = pattern.matcher(caUri1);
        assertTrue(m.find());
        String caId1 = m.group(1);
        assertEquals(caId1, compActivity1.getId());

        MvcResult result2 = mockMvc.perform(post("/comp-activities")
                .content(objectMapper.writeValueAsString(compActivity2)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        String caUri2 = result2.getResponse().getHeaderValue("Location").toString();
        m = pattern.matcher(caUri2);
        assertTrue(m.find());
        String caId2 = m.group(1);
        assertEquals(caId2, compActivity2.getId());

        mockMvc.perform(get("/comp-activities/{0}", caId1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.comp_activity_id", is(caId1)));

        mockMvc.perform(get("/comp-activities/{0}", caId2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.comp_activity_id", is(caId2)));

        mockMvc.perform(get("/comp-activities"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("comp-activities")))
                .andExpect(jsonPath("$.total", is(2)));
    }
}
