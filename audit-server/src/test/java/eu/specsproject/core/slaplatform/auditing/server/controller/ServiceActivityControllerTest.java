package eu.specsproject.core.slaplatform.auditing.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fakemongo.Fongo;
import com.mongodb.DB;
import eu.specs.datamodel.enforcement.ServiceActivity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:audit-server-context-test.xml")
public class ServiceActivityControllerTest {
    private static final Logger logger = LogManager.getLogger(ServiceActivityControllerTest.class);

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Autowired
    public Fongo fongo;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
        this.objectMapper = new ObjectMapper();
    }

    @After
    public void tearDown() throws Exception {
        for (DB db : fongo.getUsedDatabases()) {
            logger.trace("Dropping database " + db.getName());
            db.dropDatabase();
        }
    }

    @Test
    public void testApi() throws Exception {

        ServiceActivity ServiceActivity1 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/service-activity.json"), ServiceActivity.class);
        ServiceActivity ServiceActivity2 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/service-activity.json"), ServiceActivity.class);
        ServiceActivity2.setId(UUID.randomUUID().toString());

        MvcResult result1 = mockMvc.perform(post("/serv-activities")
                .content(objectMapper.writeValueAsString(ServiceActivity1)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        String caUri1 = result1.getResponse().getHeaderValue("Location").toString();
        Pattern pattern = Pattern.compile("/([\\w-]+)$");
        Matcher m = pattern.matcher(caUri1);
        assertTrue(m.find());
        String saId1 = m.group(1);
        assertEquals(saId1, ServiceActivity1.getId());

        MvcResult result2 = mockMvc.perform(post("/serv-activities")
                .content(objectMapper.writeValueAsString(ServiceActivity2)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        String caUri2 = result2.getResponse().getHeaderValue("Location").toString();
        m = pattern.matcher(caUri2);
        assertTrue(m.find());
        String saId2 = m.group(1);
        assertEquals(saId2, ServiceActivity2.getId());

        mockMvc.perform(get("/serv-activities/{0}", saId1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.serv_activity_id", is(saId1)));

        mockMvc.perform(get("/serv-activities/{0}", saId2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.serv_activity_id", is(saId2)));

        mockMvc.perform(get("/serv-activities"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("serv-activities")))
                .andExpect(jsonPath("$.total", is(2)));
    }
}
