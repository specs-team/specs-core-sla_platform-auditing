package eu.specsproject.core.slaplatform.auditing.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.CompActivity;
import eu.specsproject.core.slaplatform.auditing.server.TestParent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:audit-server-context-test.xml")
public class CompActivityServiceTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(CompActivityServiceTest.class);

    @Autowired
    private CompActivityService caService;

    @Test
    public void testService() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        CompActivity compActivity1 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/comp-activity.json"), CompActivity.class);

        String caId1 = caService.save(compActivity1);

        CompActivity compActivity1a = caService.findById(caId1);
        assertEquals(compActivity1a.getId(), caId1);
        assertEquals(compActivity1a.getSlaId(), compActivity1.getSlaId());

        CompActivity compActivity2 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/comp-activity.json"), CompActivity.class);
        compActivity2.setId(UUID.randomUUID().toString());
        String caId2 = caService.save(compActivity2);

        CompActivityService.Filter filter = new CompActivityService.Filter();
        List<CompActivity> caIdList = caService.findAll(filter);
        assertEquals(caIdList.size(), 2);
        assertEquals(caIdList.get(0).getId(), caId1);
        assertEquals(caIdList.get(1).getId(), caId2);
    }
}