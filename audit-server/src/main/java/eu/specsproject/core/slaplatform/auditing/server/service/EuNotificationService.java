package eu.specsproject.core.slaplatform.auditing.server.service;

import eu.specs.datamodel.enforcement.EuNotification;
import eu.specsproject.core.slaplatform.auditing.server.repository.EuNotificationRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EuNotificationService {
    private static final Logger logger = LogManager.getLogger(EuNotificationService.class);

    @Autowired
    private EuNotificationRepository caRepository;

    public String save(EuNotification euNotification) {
        caRepository.save(euNotification);
        return euNotification.getId();
    }

    public EuNotification findById(String caId) {
        return caRepository.findById(caId);
    }

    public List<EuNotification> findAll(Filter filter) {
        return caRepository.findAll(filter);
    }

    public static class Filter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
