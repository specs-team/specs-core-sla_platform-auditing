package eu.specsproject.core.slaplatform.auditing.server.exceptions;

public class AuditingException extends Exception {

    public AuditingException() {
        super();
    }

    public AuditingException(String message) {
        super(message);
    }

    public AuditingException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
