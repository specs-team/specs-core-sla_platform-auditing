package eu.specsproject.core.slaplatform.auditing.server.repository;

import eu.specs.datamodel.enforcement.EuNotification;
import eu.specsproject.core.slaplatform.auditing.server.service.EuNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EuNotificationRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public EuNotification findById(String notifId) {
        return mongoTemplate.findById(notifId, EuNotification.class);
    }

    public List<EuNotification> findAll(EuNotificationService.Filter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, EuNotification.class);
    }

    public void save(EuNotification euNotification) {
        mongoTemplate.save(euNotification);
    }
}
