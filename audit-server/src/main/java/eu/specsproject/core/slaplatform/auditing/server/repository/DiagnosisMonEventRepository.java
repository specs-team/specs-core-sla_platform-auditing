package eu.specsproject.core.slaplatform.auditing.server.repository;

import eu.specs.datamodel.enforcement.DiagnosisMonEvent;
import eu.specsproject.core.slaplatform.auditing.server.service.DiagnosisMonEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DiagnosisMonEventRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public DiagnosisMonEvent findById(String dmeId) {
        return mongoTemplate.findById(dmeId, DiagnosisMonEvent.class);
    }

    public List<DiagnosisMonEvent> findAll(DiagnosisMonEventService.Filter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, DiagnosisMonEvent.class);
    }

    public void save(DiagnosisMonEvent diagnosisMonEvent) {
        mongoTemplate.save(diagnosisMonEvent);
    }
}
