package eu.specsproject.core.slaplatform.auditing.server.repository;

import eu.specs.datamodel.enforcement.CompActivity;
import eu.specsproject.core.slaplatform.auditing.server.service.CompActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public CompActivity findById(String caId) {
        return mongoTemplate.findById(caId, CompActivity.class);
    }

    public List<CompActivity> findAll(CompActivityService.Filter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, CompActivity.class);
    }

    public void save(CompActivity compActivity) {
        mongoTemplate.save(compActivity);
    }
}
