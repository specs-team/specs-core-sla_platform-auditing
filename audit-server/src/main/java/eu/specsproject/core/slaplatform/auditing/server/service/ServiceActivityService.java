package eu.specsproject.core.slaplatform.auditing.server.service;

import eu.specs.datamodel.enforcement.ServiceActivity;
import eu.specsproject.core.slaplatform.auditing.server.repository.ServiceActivityRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceActivityService {
    private static final Logger logger = LogManager.getLogger(ServiceActivityService.class);

    @Autowired
    private ServiceActivityRepository saRepository;

    public String save(ServiceActivity serviceActivity) {
        saRepository.save(serviceActivity);
        return serviceActivity.getId();
    }

    public ServiceActivity findById(String caId) {
        return saRepository.findById(caId);
    }

    public List<ServiceActivity> findAll(Filter filter) {
        return saRepository.findAll(filter);
    }

    public static class Filter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
