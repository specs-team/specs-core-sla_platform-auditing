package eu.specsproject.core.slaplatform.auditing.server.service;

import eu.specs.datamodel.enforcement.CompActivity;
import eu.specsproject.core.slaplatform.auditing.server.repository.CompActivityRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompActivityService {
    private static final Logger logger = LogManager.getLogger(CompActivityService.class);

    @Autowired
    private CompActivityRepository caRepository;

    public String save(CompActivity compActivity) {
        caRepository.save(compActivity);
        return compActivity.getId();
    }

    public CompActivity findById(String caId) {
        return caRepository.findById(caId);
    }

    public List<CompActivity> findAll(Filter filter) {
        return caRepository.findAll(filter);
    }

    public static class Filter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
