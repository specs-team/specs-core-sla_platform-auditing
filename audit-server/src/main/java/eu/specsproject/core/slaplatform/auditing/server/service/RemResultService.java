package eu.specsproject.core.slaplatform.auditing.server.service;

import eu.specs.datamodel.enforcement.RemResult;
import eu.specsproject.core.slaplatform.auditing.server.repository.RemResultRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RemResultService {
    private static final Logger logger = LogManager.getLogger(RemResultService.class);

    @Autowired
    private RemResultRepository rrRepository;

    public String save(RemResult remResult) {
        rrRepository.save(remResult);
        return remResult.getId();
    }

    public RemResult findById(String caId) {
        return rrRepository.findById(caId);
    }

    public List<RemResult> findAll(Filter filter) {
        return rrRepository.findAll(filter);
    }

    public static class Filter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
