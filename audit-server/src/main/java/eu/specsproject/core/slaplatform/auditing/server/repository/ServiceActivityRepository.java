package eu.specsproject.core.slaplatform.auditing.server.repository;

import eu.specs.datamodel.enforcement.ServiceActivity;
import eu.specsproject.core.slaplatform.auditing.server.service.ServiceActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public ServiceActivity findById(String saId) {
        return mongoTemplate.findById(saId, ServiceActivity.class);
    }

    public List<ServiceActivity> findAll(ServiceActivityService.Filter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, ServiceActivity.class);
    }

    public void save(ServiceActivity serviceActivity) {
        mongoTemplate.save(serviceActivity);
    }
}
