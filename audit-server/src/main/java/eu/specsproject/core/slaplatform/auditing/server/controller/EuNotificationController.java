package eu.specsproject.core.slaplatform.auditing.server.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.EuNotification;
import eu.specsproject.core.slaplatform.auditing.server.exceptions.ResourceNotFoundException;
import eu.specsproject.core.slaplatform.auditing.server.service.EuNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "eu-notifications")
public class EuNotificationController {
    private static final String RESOURCE_PATH = "eu-notifications/{id}";

    @Autowired
    private EuNotificationService notifService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity createEuNotification(@RequestBody EuNotification euNotification) {
        String notifId = notifService.save(euNotification);

        URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .pathSegment(RESOURCE_PATH)
                .buildAndExpand(notifId)
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAll(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        EuNotificationService.Filter filter = new EuNotificationService.Filter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<EuNotification> notifications = notifService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (EuNotification notification : notifications) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(RESOURCE_PATH)
                    .buildAndExpand(notification.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(notification.getId(), uri.toString()));
        }
        collection.setResource("eu-notifications");
        collection.setTotal(notifications.size());
        return collection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public EuNotification get(@PathVariable("id") String notifId) throws ResourceNotFoundException {
        EuNotification EuNotification = notifService.findById(notifId);
        if (EuNotification == null) {
            throw new ResourceNotFoundException(String.format("EuNotification %s cannot be found.", notifId));
        } else {
            return EuNotification;
        }
    }
}
