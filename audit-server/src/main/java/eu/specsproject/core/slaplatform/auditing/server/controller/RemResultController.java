package eu.specsproject.core.slaplatform.auditing.server.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.RemResult;
import eu.specsproject.core.slaplatform.auditing.server.exceptions.ResourceNotFoundException;
import eu.specsproject.core.slaplatform.auditing.server.service.RemResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "rem-results")
public class RemResultController {
    private static final String RESOURCE_PATH = "rem-results/{id}";

    @Autowired
    private RemResultService rrService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity createRemResult(@RequestBody RemResult remResult) {
        String rrId = rrService.save(remResult);

        URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .pathSegment(RESOURCE_PATH)
                .buildAndExpand(rrId)
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAll(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        RemResultService.Filter filter = new RemResultService.Filter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<RemResult> remResults = rrService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (RemResult remResult : remResults) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(RESOURCE_PATH)
                    .buildAndExpand(remResult.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(remResult.getId(), uri.toString()));
        }
        collection.setResource("rem-results");
        collection.setTotal(remResults.size());
        return collection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public RemResult get(@PathVariable("id") String rrId) throws ResourceNotFoundException {
        RemResult RemResult = rrService.findById(rrId);
        if (RemResult == null) {
            throw new ResourceNotFoundException(String.format("Rem result %s cannot be found.", rrId));
        } else {
            return RemResult;
        }
    }
}
