package eu.specsproject.core.slaplatform.auditing.server.repository;

import eu.specs.datamodel.enforcement.RemResult;
import eu.specsproject.core.slaplatform.auditing.server.service.RemResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RemResultRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public RemResult findById(String rrId) {
        return mongoTemplate.findById(rrId, RemResult.class);
    }

    public List<RemResult> findAll(RemResultService.Filter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, RemResult.class);
    }

    public void save(RemResult remResult) {
        mongoTemplate.save(remResult);
    }
}
