package eu.specsproject.core.slaplatform.auditing.server.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.DiagnosisMonEvent;
import eu.specsproject.core.slaplatform.auditing.server.exceptions.ResourceNotFoundException;
import eu.specsproject.core.slaplatform.auditing.server.service.DiagnosisMonEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "diag-mon-events")
public class DiagnosisMonEventController {
    private static final String RESOURCE_PATH = "diag-mon-events/{id}";

    @Autowired
    private DiagnosisMonEventService dmeService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity createDiagnosisMonEvent(@RequestBody DiagnosisMonEvent diagnosisMonEvent) {
        String caId = dmeService.save(diagnosisMonEvent);

        URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .pathSegment(RESOURCE_PATH)
                .buildAndExpand(caId)
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAll(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        DiagnosisMonEventService.Filter filter = new DiagnosisMonEventService.Filter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<DiagnosisMonEvent> dmeList = dmeService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (DiagnosisMonEvent diagnosisMonEvent : dmeList) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(RESOURCE_PATH)
                    .buildAndExpand(diagnosisMonEvent.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(diagnosisMonEvent.getId(), uri.toString()));
        }
        collection.setResource("diag-mon-events");
        collection.setTotal(dmeList.size());
        return collection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public DiagnosisMonEvent get(@PathVariable("id") String dmeId) throws ResourceNotFoundException {
        DiagnosisMonEvent diagnosisMonEvent = dmeService.findById(dmeId);
        if (diagnosisMonEvent == null) {
            throw new ResourceNotFoundException(String.format("Diagnosis monitoring event %s cannot be found.", dmeId));
        } else {
            return diagnosisMonEvent;
        }
    }
}
