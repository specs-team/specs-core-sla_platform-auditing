package eu.specsproject.core.slaplatform.auditing.server.service;

import eu.specs.datamodel.enforcement.DiagnosisMonEvent;
import eu.specsproject.core.slaplatform.auditing.server.repository.DiagnosisMonEventRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiagnosisMonEventService {
    private static final Logger logger = LogManager.getLogger(DiagnosisMonEventService.class);

    @Autowired
    private DiagnosisMonEventRepository dmeRepository;

    public String save(DiagnosisMonEvent diagnosisMonEvent) {
        dmeRepository.save(diagnosisMonEvent);
        return diagnosisMonEvent.getId();
    }

    public DiagnosisMonEvent findById(String caId) {
        return dmeRepository.findById(caId);
    }

    public List<DiagnosisMonEvent> findAll(Filter filter) {
        return dmeRepository.findAll(filter);
    }

    public static class Filter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
