package eu.specsproject.core.slaplatform.auditing.server.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.CompActivity;
import eu.specsproject.core.slaplatform.auditing.server.exceptions.ResourceNotFoundException;
import eu.specsproject.core.slaplatform.auditing.server.service.CompActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "comp-activities")
public class CompActivityController {
    private static final String RESOURCE_PATH = "comp-activities/{id}";

    @Autowired
    private CompActivityService caService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity createCompActivity(@RequestBody CompActivity compActivity) {
        String caId = caService.save(compActivity);

        URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .pathSegment(RESOURCE_PATH)
                .buildAndExpand(caId)
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAll(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        CompActivityService.Filter filter = new CompActivityService.Filter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<CompActivity> compActivities = caService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (CompActivity compActivity : compActivities) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(RESOURCE_PATH)
                    .buildAndExpand(compActivity.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(compActivity.getId(), uri.toString()));
        }
        collection.setResource("comp-activities");
        collection.setTotal(compActivities.size());
        return collection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public CompActivity get(@PathVariable("id") String compActId) throws ResourceNotFoundException {
        CompActivity CompActivity = caService.findById(compActId);
        if (CompActivity == null) {
            throw new ResourceNotFoundException(String.format("Component activity %s cannot be found.", compActId));
        } else {
            return CompActivity;
        }
    }
}
