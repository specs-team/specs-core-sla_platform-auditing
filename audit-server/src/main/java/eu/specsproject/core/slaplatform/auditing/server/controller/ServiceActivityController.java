package eu.specsproject.core.slaplatform.auditing.server.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.ServiceActivity;
import eu.specsproject.core.slaplatform.auditing.server.exceptions.ResourceNotFoundException;
import eu.specsproject.core.slaplatform.auditing.server.service.ServiceActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "serv-activities")
public class ServiceActivityController {
    private static final String RESOURCE_PATH = "serv-activities/{id}";

    @Autowired
    private ServiceActivityService saService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity createServiceActivity(@RequestBody ServiceActivity serviceActivity,
                                                HttpServletRequest request) {
        String saId = saService.save(serviceActivity);

        URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .pathSegment(RESOURCE_PATH)
                .buildAndExpand(saId)
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAll(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        ServiceActivityService.Filter filter = new ServiceActivityService.Filter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<ServiceActivity> serviceActivities = saService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (ServiceActivity serviceActivity : serviceActivities) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(RESOURCE_PATH)
                    .buildAndExpand(serviceActivity.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(serviceActivity.getId(), uri.toString()));
        }
        collection.setResource("serv-activities");
        collection.setTotal(serviceActivities.size());
        return collection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ServiceActivity get(@PathVariable("id") String serviceActId) throws ResourceNotFoundException {
        ServiceActivity ServiceActivity = saService.findById(serviceActId);
        if (ServiceActivity == null) {
            throw new ResourceNotFoundException(String.format("Service activity %s cannot be found.", serviceActId));
        } else {
            return ServiceActivity;
        }
    }
}
