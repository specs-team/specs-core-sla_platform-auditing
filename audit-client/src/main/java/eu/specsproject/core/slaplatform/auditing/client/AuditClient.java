package eu.specsproject.core.slaplatform.auditing.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class AuditClient {
    private static final Logger logger = LogManager.getLogger(AuditClient.class);
    private WebTarget auditServerTarget;

    public AuditClient(String auditServerAddress) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        auditServerTarget = client.target(auditServerAddress);
    }

    public void logComponentActivity(CompActivity compActivity) {
        try {
            logger.trace("Storing CompActivity audit event {}...", compActivity.getId());
            auditServerTarget
                    .path("comp-activities")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(compActivity));

            logger.debug("CompActivity audit event stored successfully.", compActivity.getId());

        } catch (Exception e) {
            logger.error(String.format(
                    "Failed to store CompActivity audit event %s: %s", compActivity.getId(), e.getMessage()), e);
        }
    }

    public void logComponentActivity(String componentId, CompActivity.ComponentState compState,
                                     String slaId, SlaState slaState) {

        CompActivity compActivity = new CompActivity();
        compActivity.setId(UUID.randomUUID().toString());
        compActivity.setSlaId(slaId);
        compActivity.setSlaState(slaState);
        compActivity.setComponentId(componentId);
        compActivity.setComponentState(compState);
        compActivity.setCreationTime(new Date());

        logComponentActivity(compActivity);
    }

    public void logServiceActivity(ServiceActivity serviceActivity) {
        try {
            logger.trace("Storing ServiceActivity audit event {}...", serviceActivity.getId());
            auditServerTarget
                    .path("service-activities")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(serviceActivity));

            logger.debug("ServiceActivity audit event stored successfully.", serviceActivity.getId());

        } catch (Exception e) {
            logger.error(String.format(
                    "Failed to store ServiceActivity audit event %s: %s", serviceActivity.getId(), e.getMessage()), e);
        }
    }

    public void logServiceActivity(String slaId, SlaState slaState, String implPlanId, List<String> services) {

        ServiceActivity serviceActivity = new ServiceActivity();
        serviceActivity.setId(UUID.randomUUID().toString());
        serviceActivity.setSlaId(slaId);
        serviceActivity.setSlaState(slaState);
        serviceActivity.setImplPlanId(implPlanId);
        serviceActivity.setServices(services);
        serviceActivity.setCreationTime(new Date());

        logServiceActivity(serviceActivity);
    }

    public void logDiagnosisMonEvent(DiagnosisMonEvent diagnosisMonEvent) {
        try {
            logger.trace("Storing DiagnosisMonEvent audit event {}...", diagnosisMonEvent.getId());
            auditServerTarget
                    .path("diag-mon-events")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(diagnosisMonEvent));

            logger.debug("DiagnosisMonEvent audit event stored successfully.", diagnosisMonEvent.getId());

        } catch (Exception e) {
            logger.error(String.format(
                    "Failed to store DiagnosisMonEvent audit event %s: %s", diagnosisMonEvent.getId(), e.getMessage()), e);
        }
    }

    public void logEuNotification(EuNotification euNotification) {
        try {
            logger.trace("Storing EuNotification audit event {}...", euNotification.getId());
            auditServerTarget
                    .path("eu-notifications")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(euNotification));

            logger.debug("EuNotification audit event stored successfully.", euNotification.getId());

        } catch (Exception e) {
            logger.error(String.format(
                    "Failed to store EuNotification audit event %s: %s", euNotification.getId(), e.getMessage()), e);
        }
    }

    public void logRemResult(RemResult remResult) {
        try {
            logger.trace("Storing RemResult audit event {}...", remResult.getId());
            auditServerTarget
                    .path("rem-results")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(remResult));

            logger.debug("RemResult audit event stored successfully.", remResult.getId());

        } catch (Exception e) {
            logger.error(String.format(
                    "Failed to store RemResult audit event %s: %s", remResult.getId(), e.getMessage()), e);
        }
    }
}
