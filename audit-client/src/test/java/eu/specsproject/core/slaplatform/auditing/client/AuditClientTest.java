package eu.specsproject.core.slaplatform.auditing.client;

import eu.specs.datamodel.enforcement.CompActivity;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class AuditClientTest {

    @Test
    public void test() {
        AuditClient auditClient = new AuditClient("http://localhost:8080/audit-server");
        CompActivity compActivity = new CompActivity();
        compActivity.setId("test1");
        compActivity.setSlaId("12345");
        auditClient.logComponentActivity(compActivity);
    }

}